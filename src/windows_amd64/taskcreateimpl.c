#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <task.h>
#include "taskctx.h"
#include "../taskimpl.h"

struct task *
taskcreateimpl(void (*f)(void *), void *arg, size_t stacksize)
{
	size_t stacksize2 = (stacksize + 15) & ~(size_t) 15;
	char *stack = _aligned_malloc(stacksize2, 16);
	if (stack == NULL) {
		abort();
	}
	char *sp = stack + stacksize2;
	struct task *t = malloc(sizeof(*t));
	if (t == NULL) {
		abort();
	}
	t->exited = false;
	t->ctx = (taskctx) {
		.rsp = (uint64_t) sp,
		.rip = (uint64_t) taskcreateimpl,
	};
	t->res = stack;
	t->f = f;
	t->arg = arg;
	return t;
}

void
taskfree(struct task *t)
{
	_aligned_free(t->res);
	free(t);
}
