.text

.globl taskyieldimpl
taskyieldimpl:
	movq %rbx, 0(%rcx)
	movq %rsi, 8(%rcx)
	movq %rdi, 16(%rcx)
	movq %rbp, 24(%rcx)
	popq %rax
	movq %rsp, 32(%rcx)
	movq %r12, 40(%rcx)
	movq %r13, 48(%rcx)
	movq %r14, 56(%rcx)
	movq %r15, 64(%rcx)
	movq %rax, 72(%rcx)
	movdqu %xmm6, 80(%rcx)
	movdqu %xmm7, 96(%rcx)
	movdqu %xmm8, 112(%rcx)
	movdqu %xmm9, 128(%rcx)
	movdqu %xmm10, 144(%rcx)
	movdqu %xmm11, 160(%rcx)
	movdqu %xmm12, 176(%rcx)
	movdqu %xmm13, 192(%rcx)
	movdqu %xmm14, 208(%rcx)
	movdqu %xmm15, 224(%rcx)
	movq 0(%rdx), %rbx
	movq 8(%rdx), %rsi
	movq 16(%rdx), %rdi
	movq 24(%rdx), %rbp
	movq 32(%rdx), %rsp
	movq 40(%rdx), %r12
	movq 48(%rdx), %r13
	movq 56(%rdx), %r14
	movq 64(%rdx), %r15
	movq 72(%rdx), %rax
	movdqu 80(%rdx), %xmm6
	movdqu 96(%rdx), %xmm7
	movdqu 112(%rdx), %xmm8
	movdqu 128(%rdx), %xmm9
	movdqu 144(%rdx), %xmm10
	movdqu 160(%rdx), %xmm11
	movdqu 176(%rdx), %xmm12
	movdqu 192(%rdx), %xmm13
	movdqu 208(%rdx), %xmm14
	movdqu 224(%rdx), %xmm15
	jmp *%rax
