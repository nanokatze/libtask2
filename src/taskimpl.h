typedef struct taskctx taskctx;

struct task {
	struct task *succ;
	bool exited;
	taskctx ctx;

	/* cold */

	void *res;
	void (*f)(void *);
	void *arg;
};

extern struct task *curtask;

void taskep(void);
struct task * taskcreateimpl(void (*f)(void *), void *arg, size_t stacksize);
void taskfree(struct task *);
void taskyieldimpl(taskctx *from, taskctx *to);
