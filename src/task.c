#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <task.h>
#include <taskctx.h>
#include "taskimpl.h"

static void taskmainshim(void *a);

struct task *curtask;

void
taskcreate(void (*f)(void *), void *arg, size_t stacksize)
{
	struct task *t = taskcreateimpl(f, arg, stacksize);
	t->succ = curtask->succ;
	curtask->succ = t;
}

void
taskyield(void)
{
	struct task *t = curtask->succ;
	while (t != curtask && t->exited) {
		struct task *succ = t->succ;
		curtask->succ = succ;
		taskfree(t);
		t = succ;
	}
	taskctx *from = &curtask->ctx;
	taskctx *to = &t->ctx;
	curtask = t;
	taskyieldimpl(from, to);
}

void
taskexit(void)
{
	curtask->exited = true;
	for (;;) {
		taskyield();
	}
}

void
taskep(void)
{
	void (*f)(void *) = curtask->f;
	void *arg = curtask->arg;
	f(arg);
	taskexit();
}

struct args {
	int c;
	char **v;
};

static bool shutdown = false;

int
main(int argc, char **argv)
{
	struct task task0 = {
		.succ = &task0,
	};
	curtask = &task0;

	struct args a = {
		.c = argc,
		.v = argv,
	};
	taskcreate(taskmainshim, &a, 0x8000);

	while (!shutdown) {
		taskyield();
	}

	assert(curtask == &task0);

	struct task *t = task0.succ;
	while (t != &task0) {
		struct task *succ = t->succ;
		taskfree(t);
		t = succ;
	}
	return 0;
}

static void
taskmainshim(void *a)
{
	struct args *a2 = a;
	taskmain(a2->c, a2->v);
	shutdown = true;
}
