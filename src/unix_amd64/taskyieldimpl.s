.text

.globl taskyieldimpl
taskyieldimpl:
	movq %rbx, 0(%rdi)
	movq %rbp, 8(%rdi)
	popq %rax
	movq %rsp, 16(%rdi)
	movq %r12, 24(%rdi)
	movq %r13, 32(%rdi)
	movq %r14, 40(%rdi)
	movq %r15, 48(%rdi)
	movq %rax, 56(%rdi)
	movq 0(%rsi), %rbx
	movq 8(%rsi), %rbp
	movq 16(%rsi), %rsp
	movq 24(%rsi), %r12
	movq 32(%rsi), %r13
	movq 40(%rsi), %r14
	movq 48(%rsi), %r15
	movq 56(%rsi), %rax
	jmp *%rax
