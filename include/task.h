/*
 * #include <stddef.h>
 */

void taskmain(int argc, char **argv);
void taskcreate(void (*f)(void *), void *arg, size_t stacksize);
void taskyield(void);
void taskexit(void);
